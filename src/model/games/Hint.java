package model.games;

/**
 * 
 * @author Stefano Benelli
 * This enum represents the list of all the possibile Hints
 */
public enum Hint {
	EMPTY,
	HIT, //white
	WRONGPOS //black
}
