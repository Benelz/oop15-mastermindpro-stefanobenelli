package model.games;

/**
 * 
 * @author Stefano Benelli
 * This class represent a concrete implementation of Round
 */
public class RoundImpl extends RoundBase {

	private static final long serialVersionUID = -4395625567472392381L;

	public RoundImpl(int noChoices) {
		super(noChoices);
	}
}
