package view.interfaces;

import javax.swing.JFrame;

public abstract class ViewBase<T> extends JFrame implements View<T> {

	private static final long serialVersionUID = -4121401971584567810L;
}
