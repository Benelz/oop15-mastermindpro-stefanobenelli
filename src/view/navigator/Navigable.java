package view.navigator;

public interface Navigable {
	void setVisible(boolean value);
}
